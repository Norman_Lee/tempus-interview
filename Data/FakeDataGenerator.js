var jsf = require('json-schema-faker');
var jsonfile = require('jsonfile');


jsf.extend('faker', function(){
	return require('faker');
})

var PatientSchema = {
	type: 'object',
	properties: {
		patient_id : {
			$ref: '#/definitions/positiveInt'
		},
		name: {
			type: 'string',
			faker: 'name.findName'
		},
		age: {
			type: 'integer',
			minimum: 0,
			maximum: 110
		},
		emailAddress: {
			type: 'string',
			faker: 'internet.email'
		},
		mailingAddress:{
			type: 'string',
			faker: 'address.streetAddress'
		},
		phoneNumber: {
			type: 'string',
			faker: 'phone.phoneNumberFormat'
		},
    doctors:{
      type: 'array',
      items: {
        type: 'integer',
        minimum: 1,
        maximum: 6
      },
      minItems: 3,
      uniqueItems: true
    },
		appointments: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          date: {
            type: 'string',
            faker: 'date.past'
          },
          description: {
            type: 'string',
            faker: 'random.words'
          },
          doctor_id: {
            $ref: '#/definitions/positiveInt'
          },
          doctorName:{
            type: 'string',
            faker: 'name.findName'
          },
          appointmentType:{
            type: 'string',
            faker: {
              'random.arrayElement' :[["pending", "past", "future"]]
            }
          }
        },
        required: ['date', 'description', 'doctor_id', 'doctorName', 'appointmentType']
      },
      minItems: 15,
      uniqueItems: true
    },
    medicalRecords: { 
      type: 'array',
      items: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            faker: 'random.words'
          },
          url: {
            type: 'string',
            faker: 'internet.url'
          }
        },
        required: ['name', 'url']
      },
      minItems: 4,
      uniqueItems: true
    }
  },
	required: ['patient_id', 'name', 'age', 'emailAddress', 'mailingAddress', 'phoneNumber','doctors' ,'appointments','medicalRecords'],
	definitions: {
		positiveInt: {
			type: 'integer',
			minimum: 1,
			maximum: 5,
			exclusiveMinimum: true
		}
	}
}


var DoctorSchema = {
	type: 'object',
	properties: {
		doctor_id : {
			$ref: '#/definitions/positiveInt'
		},
		name: {
			type: 'string',
			faker: 'name.findName'
		},
		age: {
			type: 'integer',
			minimum: 0,
			maximum: 110
		},
		emailAddress: {
			type: 'string',
			faker: 'internet.email'
		},
		mailingAddress:{
			type: 'string',
			faker: 'address.streetAddress'
		},
		phoneNumber: {
			type: 'string',
			faker: 'phone.phoneNumberFormat'
		}
	},
	required: ['doctor_id', 'name', 'age', 'emailAddress', 'mailingAddress', 'phoneNumber'],
	definitions: {
		positiveInt: {
			type: 'integer',
			minimum: 1,
			maximum: 5,
			exclusiveMinimum: true
		}
	}
}


let patientSamples = [];

var patientPromise = jsf.resolve(PatientSchema).then(function(sample){
  sample.patient_id = 1;

  patientSamples.push(sample);
  jsonfile.writeFile('./Patients.json', sample, function(err){
    console.log(err)
  })
})


var patientPromises = [];
for(var i = 2; i < 6; i++){
  
  let patientId = i;
  patientPromises.push(
    jsf.resolve(PatientSchema).then(function(sample){
      sample.patient_id = patientId;
      patientSamples.push(sample);
      jsonfile.writeFile('./Patients.json', sample, {flag: 'a'}, function(err){
        console.log(err)
      })
    })
  )
}

patientPromises.push(patientPromise);
console.log(patientPromises);

Promise.all(patientPromises).then(() => {
  jsf.resolve(DoctorSchema).then(function(sample){
    sample.doctor_id = 1;
    
    sample.patients = []
    patientSamples.map((patientSample) => {
      if(patientSample.doctors.includes(1)){
        sample.patients.push({ patient_id: patientSample.patient_id, patientName: patientSample.name  })
      }
    }) 

    jsonfile.writeFile('./Doctors.json', sample, function(err){
      console.log(err)
    })
  })

  for(var i = 2; i < 6; i++){
    let doctorId = i; 
    jsf.resolve(DoctorSchema).then(function(sample){
      sample.doctor_id = doctorId;

      sample.patients = []
      patientSamples.map((patientSample) => {
        if(patientSample.doctors.includes(doctorId)){
          sample.patients.push({ patient_id: patientSample.patient_id, patientName: patientSample.name })
        }
      }) 
      
      jsonfile.writeFile('./Doctors.json', sample, {flag: 'a'}, function(err){
        console.log(err)
      }) 
    })
  }
})
