# Tempus Interview

##Running the app

Assuming that you are in the root directory of project

```bash

cd RestAPI
node server.js

cd React-App
yarn run dev

```

As long as hot reloading is working you guys will need to change the app.jsx render function to render out the different
views as I couldn't get to routing and integrating the views together.

You can also try to run my schema writing script to see if you can generate random data in json files that you can port into the corresponding collections on mongodb.

##Design Decisions

Hey guys, as you are probably aware by now, there were a lot of things that I had wanted to do and not enough time to do it,
so hopefully I can walk you through some of the decisions that I made while designing this application from front-end to back-end instead with the intention of clearing up what had happened.

###Front-End

####UX Design

Since this was going to be an app for essentially scheduling appointments as well as for doctors to organize themselves around what was happening around the day, I figured that two different views for viewing what was going on for the patient made the most sense to me.

Essentially I can break it down to objectives for what each party wanted to accomplish with the application.

Doctors:

Doctors are generally very busy people who have to work through a lot of patients everyday -- so helping them context switch for which patients they are seeing and when should help them a lot without getting in their way. Any way to help them organize their own thoughts so that they can better serve their patients would help a lot.

1. Accept or reject appointments scheduled by patients who are scheduling time with them -- otherwise leaving them hanging would eventually leave a terrible experience with the doctor
2. Get context around who this patient is, important indicators for what issues they are facing if any.
3. Since people read from left to right -- assumption here is that we're only based in America, the upcoming appointments are listed on the left for the doctor.
4. Contacting the patient if needed to clarify some things.

So I did my best to organize the information in a way where these priorities would be met for the doctor patient view

Patients:

Patients, depending on the degree or severity of when they need that appointment scheduled should be able to request certain time slots to be added. So their concerns can revolve around time and location and providing any necessary information to have doctors help them out the best way that they can.

Assuming that this might be a doctor-sharing platform

1. Able to pick which doctors best suit their needs for concerns they are having -- there may be a need to match based on symptoms on which doctors are best suited to handle the situation (gets into webmd type of things) also an assumption
2. Able to pick out a time that suits their needs
3. Able to upload medical records as needed

So the patientView jsx should have things organized in a way where they can immediately know how to schedule an appointment


In terms of experience the Search and the Login are corrollary and simple in this case (hopefully). Doctors just need to search for patients that they are concerned with or to go through and find patients with pending appointments that need to approved.

#### Front End Technology Stack

I'll separate it out into intial thoughts and implementation.

Initial Thoughts:

If this application was at scale, I would've liked to see this stack:

* React -- Since I was building two similar views, and breaking down things into components helps when application gets large
* Redux -- For querying and allowing polling of information to happen 
* GraphQl -- Level of indirection so the API layer can increase in size or include newer fields without impacting the front end application too much
* Materialize -- Design language / more interesting css framework than bootstrap (imo) 

* Webpack -- For code splitting and bundling

But since I didn't have time to do all of that I chose this for an implementation:

* React -- Similar views for patients, so components would help here
* Axios -- Related http library used for react
* Materialize -- For styling purposes
* Webpack -- For bundling and deploying

####To-Dos for front-end

From just working on the front end, there are still a few threads that I have left hanging probably because of some overestimating on my end.

* Routing between views -- I have yet to create a routing mechanism or use react-router with the webpack-dev-server to serve up something that works

* Modals for scheduling appointments

* Search functionality within the SPA

* Centering the Login Page

* TDD


###Back-End

####Back End Design

Intial Thoughts:

The stack was going to be something like this to handle the different types of data, concerns included securing the medical record data, resolving appointment conflicts, and a choice between organizing the data into schemas for SQL or to use a NoSQL solution. Allowing APIs to be added if scope increases, and availibility for the whole organization. (Worst case is we're the only provider and let's say 25% of Anmerica runs on this, and no one keeps backups)

The initial stack looked something like this:

* SparkJava on Docker -- API layer with containerization for cloud compatibility
* Redis -- Caching layer for quick and easy data structures for scheduling appointments (Concerns include whether solving for appointment conflicts would be easy here or harder from tech selection)
* PostgreSQL or Mongo -- For transacting appointments (Perhaps)
* Hadoop -- A distributed filesystem hopefully to be used like S3 to secure and upload personal and private medical records.

Obviously no time for that (Was 4 days in):

* Express --Serving static files, loading static files, API layer
* MongoDB --Storage for Appointments /Patients /Doctors 


I could only get to writing a script to generate the schemas for Mongo as needed and building out the express endpoints, I had spent a little too much time planning and severely overestimated what I could do in a week with a daytime software job.

####To-Dos for Backend

* Integrate between front end and data layer -- Checked for mongo connectivity on basic localhost, but have not tested if collection creates and puts work as intended
* TDD -- Couldn't follow best practices here for developing as I was running out of time
* Express was tested to be running so hitting endpoints should work
* Including Body-Parser into express
* Doing End to End testing
* Static file storage through express for uploading medical records
* Creating a script for you guys to upload schema data into a mongodb database
