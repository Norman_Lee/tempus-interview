import React from 'react';
import axios from 'axios';
import PatientBanner from './components/PatientBanner.jsx';
import PatientSubBanner from './components/PatientSubBanner.jsx';
import MedicalRecords from './components/MedicalRecords.jsx';
import NavigationBar from './components/NavigationBar.jsx';
import moment from 'moment';
import PastAppointments from './components/PastAppointments.jsx'
import UpcomingAppointments from './components/UpcomingAppointments.jsx'
import PendingAppointments from './components/PendingAppointments.jsx'
import {Row, Col, Card} from 'react-materialize'

export default class DoctorPatientView extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			patient: { 
				name: '',
				age: '',
				mailingAddress: '',
				phoneNumber: '',
				emailAddress: '',
				profileUrl: ''
      },

      medicalRecords: [],
      
			pastAppointments: [],
			pendingAppointments: [],
			futureAppointments: [],
      
    }
    this.cancelAppointment = this.cancelAppointment.bind(this);
    this.approveAppointment = this.approveAppointment.bind(this);
    this.createNewAppointment = this.createNewAppointment.bind(this);
  }

	shouldComponentUpdate(){
		return true;
	}
			
	componentDidMount(){
    let token = localStorage.getItem('access_token')      
    axios.get('http://localhost:3000/doctors/'+ this.props.location.state.doctorId +'/patients/' + this.props.location.state.patientId, { headers: {'Authorization' : 'Bearer ' + token}})
			.then((response) => {
				//Do some conversion to get it into correct data state
        //console.log(response);
        let medicalRecords = response.data.patient.medicalRecords
        delete response.data.patient.medicalRecords
        this.setState({ patient: response.data.patient, medicalRecords: medicalRecords});    
      })
			.catch((error) => {
				console.log(error);
      })

     axios.get('http://localhost:3000/doctors/'+ this.props.location.state.doctorId +'/patients/' + this.props.location.state.patientId + '/appointments',  
      { headers: {'Authorization' : 'Bearer ' + token}})
      .then((response) => {
        //console.log(response.data);
        this.setState({ 
          pastAppointments: response.data.appointments.past,
          pendingAppointments: response.data.appointments.pending,
          futureAppointments: response.data.appointments.future
        });
      }).catch((error) => {
        console.log(error);
      })
  }

  cancelAppointment(appointmentId, type, index){  
    let token = localStorage.getItem('access_token');
    axios.delete('http://localhost:3000/doctors/'+ this.props.location.state.doctorId +'/patients/' + this.props.location.state.patientId + '/appointments/' + appointmentId , 
      { headers: {'Authorization' : 'Bearer ' + token}})
			.then((response) => {
				//Do some conversion to get it into correct data state
        //console.log(response);
        if(type === "pending"){
          console.log("Inside pending loop");
          let newPendingAppointments = this.state.pendingAppointments;
          newPendingAppointments.splice(index, 1);

          this.setState({ pendingAppointments: newPendingAppointments  });
        } else if(type === "future"){ 
          console.log("Inside future loop")
          let newFutureAppointments = this.state.futureAppointments;
          newFutureAppointments.splice(index, 1);

          this.setState({ futureAppointments: newFutureAppointments  });
        }
			} )
			.catch((error) => {
				console.log(error);
			})
  }

  approveAppointment(appointment, index){
    let token = localStorage.getItem('access_token');
    axios.put('http://localhost:3000/doctors/'+ this.props.location.state.doctorId +'/patients/' + this.props.location.state.patientId + '/appointments/' + appointment._id ,
      {
        _id: appointment._id,
        appointmentType: 'future',
        date: moment(appointment.date + " " + appointment.time),
        description: appointment.description,
        doctor_id: appointment.doctor_id,
        doctorName: appointment.doctorName
      },
      { headers: {'Authorization' : 'Bearer ' + token}})
			.then((response) => {
				//Do some conversion to get it into correct data state
				console.log(response);
        let newPendingAppointments = this.state.pendingAppointments;
        let newFutureAppointments = this.state.futureAppointments;
        let approvedAppointment = newPendingAppointments.splice(index, 1);
        newFutureAppointments.push(approvedAppointment[0]);
        this.setState({
          pendingAppointments: newPendingAppointments,
          futureAppointments: newFutureAppointments
        });
			} )
			.catch((error) => {
				console.log(error);
			})
 
  }
   
  createNewAppointment(date, description, handleError){
    let token = localStorage.getItem('access_token')      
    axios.post('http://localhost:3000/doctors/' + this.props.location.state.doctorId + '/patients/' + this.props.location.state.patientId + '/appointments', {
				date: date,
				description: description,
				doctor_id: this.props.location.state.doctorId,
				doctorName: 'test',
        appointmentType: 'future'
			}, { headers: {'Authorization' : 'Bearer ' + token}})
			.then((response) => {
        console.log(response);
        
        let futureAppointments = this.state.futureAppointments;

        let newAppointmentItem = {
          date: date,
          description: description,
          doctor_id: this.props.location.state.doctorId,
          doctorName: 'test',
          appointmentType: 'future'
        }

        futureAppointments.push(newAppointmentItem);

        this.setState({ futureAppointments: futureAppointments })
			})	
			.catch((error) =>{
        console.log(error);
        handleError();
			})
  }
   
  render(){
		
    const doctorId = this.props.location.state.doctorId 
    const patientId = this.props.location.state.patientId

		return (
      <div>
        <NavigationBar searchLink={true}></NavigationBar>
        <div className="container">
          <PatientBanner patient={this.state.patient}></PatientBanner>
          <PatientSubBanner patient={this.state.patient}></PatientSubBanner>
          <Card className="blue horizontal center-align appointment-bar">
            <div>
              <h6>Appointments</h6>
            </div>
          </Card>
          <Row>
            <Col l={3}>
              <UpcomingAppointments futureAppointments={ this.state.futureAppointments } cancelAppointment={ this.cancelAppointment }></UpcomingAppointments>
              <PastAppointments pastAppointments= { this.state.pastAppointments }></PastAppointments>
            </Col>
            <Col l={9}>
              <PendingAppointments pendingAppointments= { this.state.pendingAppointments } createNewAppointment={ this.createNewAppointment } approveAppointment={ this.approveAppointment } cancelAppointment={ this.cancelAppointment } ></PendingAppointments>
              <MedicalRecords doctorId= { doctorId } patientId= { patientId } medicalRecords= { this.state.medicalRecords } > </MedicalRecords>
            </Col>
          </Row>
        </div>
			</div>
			)
	} 
}
