import React from 'react';
import NavigationBar from './components/NavigationBar.jsx';
import { Slider, Slide } from 'react-materialize'

export default class LoginView extends React.Component{
	constructor(props){
		super(props);
		this.login = this.login.bind(this);
		this.logout = this.logout.bind(this);
	}	


	login(event){
		event.preventDefault();
		console.log("Userlogin was triggered");
		this.props.auth.login();
	}


	logout(event){
		event.preventDefault();
		this.props.auth.logout();
	}

	render(){
    return(
      <div>
      <Slider>
      <Slide 
        src="http://freestocks.org/fs/wp-content/uploads/2017/05/morning_dew_on_the_grass-800x533.jpg"
        title="Welcome"
        placement="right"
        >
        Here is my smol slogan 
        </Slide>         
        </Slider>
        
        <div className="container">
          <button className="btn waves-effect waves-light right" >
            Try me
          </button>
          <button className="btn waves-effect waves-light right" onClick={this.login}>
            Log-In
          </button>
        </div>
      </div>
      )
	}
}
