import React from 'react';
import PatientBanner from './components/PatientBanner.jsx';
// import PatientSubBanner from './components/PatientSubBanner.jsx';
import MedicalRecords from './components/MedicalRecords.jsx';
import axios from 'axios'
import Button from './components/Button.jsx';
import { Row, Col, Card } from 'react-materialize'; 
import PastAppointments from './components/PastAppointments.jsx'
import UpcomingAppointments from './components/UpcomingAppointments.jsx'
import PendingAppointments from './components/PendingAppointments.jsx'
import NewAppointmentButton from './components/NewAppointmentButton.jsx'
import NewMedicalRecordButton from './components/NewMedicalRecordButton.jsx';
import NavigationBar from './components/NavigationBar.jsx';

export default class PatientView extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			patient: { 
				name: '',
				age: '',
				mailingAddress: '',
				phoneNumber: '',
				emailAddress: '',
				profileUrl: ''
      },

      medicalRecords: [],
      
			pastAppointments: [],
			pendingAppointments: [],
			futureAppointments: [],
      
    }
    this.cancelAppointment = this.cancelAppointment.bind(this);
    this.createNewAppointment = this.createNewAppointment.bind(this);
    this.createNewMedicalRecord = this.createNewMedicalRecord.bind(this);
	}


	componentDidMount(){
    let token = localStorage.getItem('access_token')      
    axios.get('http://localhost:3000/patients/' + this.props.id, { headers: {'Authorization' : 'Bearer ' + token}})
			.then((response) => {
				//Do some conversion to get it into correct data state
        //console.log(response);
        let medicalRecords = response.data.patient.medicalRecords
        delete response.data.patient.medicalRecords
        this.setState({ patient: response.data.patient, medicalRecords: medicalRecords});    
      })
			.catch((error) => {
				console.log(error);
      })

     axios.get('http://localhost:3000/patients/' + this.props.id + '/appointments',  
      { headers: {'Authorization' : 'Bearer ' + token}})
      .then((response) => {
        //console.log(response.data);
        this.setState({ 
          pastAppointments: response.data.appointments.past,
          pendingAppointments: response.data.appointments.pending,
          futureAppointments: response.data.appointments.future
        });
      }).catch((error) => {
        console.log(error);
      })
	}

  cancelAppointment(appointmentId, type, index){  
    let token = localStorage.getItem('access_token');
    axios.delete('http://localhost:3000/patients/' + this.props.id + '/appointments/' + appointmentId , 
      { headers: {'Authorization' : 'Bearer ' + token}})
			.then((response) => {
				//Do some conversion to get it into correct data state
        //console.log(response);
        if(type === "pending"){
          console.log("Inside pending loop");
          let newPendingAppointments = this.state.pendingAppointments;
          newPendingAppointments.splice(index, 1);

          this.setState({ pendingAppointments: newPendingAppointments  });
        } else if(type === "future"){ 
          console.log("Inside future loop")
          let newFutureAppointments = this.state.futureAppointments;
          newFutureAppointments.splice(index, 1);

          this.setState({ futureAppointments: newFutureAppointments  });
        }
			} )
			.catch((error) => {
				console.log(error);
			})
  }

  createNewAppointment(date, description, handleError){
    let token = localStorage.getItem('access_token')      
    axios.post('http://localhost:3000/patients/' + this.props.id + '/appointments', {
				date: date,
				description: description,
				doctor_id: 'lols',
				doctorName: 'test',
        appointmentType: 'pending'
			}, { headers: {'Authorization' : 'Bearer ' + token}})
			.then((response) => {
        console.log(response);
        
        let pendingAppointments = this.state.pendingAppointments;

        let newAppointmentItem = {
          date: date,
          description: description,
          doctor_id: 'lols',
          doctorName: 'test',
          appointmentType: 'pending'
        }

        pendingAppointments.push(newAppointmentItem);

        this.setState({ pendingAppointments: pendingAppointments })
			})	
			.catch((error) =>{
        console.log(error);
        handleError();
			})
  }

  createNewMedicalRecord(formData, handleError, handleSuccess){
    let token = localStorage.getItem('access_token')      
    
    axios.post('http://localhost:3000/patients/' + this.props.id + '/medicalRecords', formData
      , { headers: {'Authorization' : 'Bearer ' + token, 
        'Content-Type' : 'multipart/form-data'}}) 
      .then((response) => { 
        console.log(response);

        let medicalRecords = this.state.medicalRecords;

        medicalRecords.push(response.data);

        this.setState({ medicalRecords: medicalRecords })
        handleSuccess();
			})	
			.catch((error) =>{
				console.log(error);
        handleError();
      })
  
  }
	render(){
		
    const patientId = this.props.id

		const medicalRecords = this.state.medicalRecords.map((record, index) =>
			<li className="collection-item center-align" key= { index } href={ record.url }> 
				{ record.name }
				<a href={ record.url } className="secondary-content"><i className="small material-icons">system_update_alt</i></a>
				<a href={ record.url } className="secondary-content"><i className="small material-icons">info_outline</i></a>
			</li>
			)


    return (
      <div> 
      <NavigationBar></NavigationBar>
			<div className="container">
      <PatientBanner patient= { this.state.patient }></PatientBanner>
				<Card className="blue horizontal center-align appointment-bar">
					<div>
					<h6>Appointments</h6>
					</div>
				</Card>
				<div className="row">
					<div className="row col l9">
						<div className="row col l6">
							<UpcomingAppointments futureAppointments= { this.state.futureAppointments } cancelAppointment={ this.cancelAppointment }></UpcomingAppointments>
						</div>
						<div className="row col l6">
							<PendingAppointments pendingAppointments= { this.state.pendingAppointments } createNewAppointment={ this.createNewAppointment } cancelAppointment={ this.cancelAppointment } patient={ true}></PendingAppointments>
						</div>
						<div className="row col l12">
              <NewAppointmentButton createNewAppointment = { this.createNewAppointment } ></NewAppointmentButton>
						</div>
						<div className="row col l12">
              <NewMedicalRecordButton  patientId={ this.props.patientId } createNewMedicalRecord={ this.createNewMedicalRecord } ></NewMedicalRecordButton>
            </div>
					</div>
					<div className="col l3">
						<PastAppointments pastAppointments= { this.state.pastAppointments }></PastAppointments>
					</div>
				</div>
					<MedicalRecords patientId={ this.props.id } medicalRecords= { this.state.medicalRecords }></MedicalRecords>
          </div>
        </div>   
			)
	}
}
