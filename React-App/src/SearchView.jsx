import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import NavigationBar from './components/NavigationBar.jsx';
import Fuse from 'fuse.js';

export default class SearchView extends React.Component{
	constructor(props){
		super(props);
		this.state = {
      patients: [],
      patientList: [],
      fuse: ''
    }

    this.setPatientView = this.setPatientView.bind(this);
	}

	componentDidMount(){
    
    let token = localStorage.getItem('access_token')      
		axios.get('http://localhost:3000/doctors/' + this.props.id + '/patients',{ headers: {'Authorization' : 'Bearer ' + token}})
			.then((response) => {
				console.log(response)
    const options = {
        shouldSort: true,
        threshold: 0.6,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength:1,
        keys:[
          "patientName"
        ]
      }
        
        this.setState( { patients:response.data.doctor.patients, patientList: response.data.doctor.patients, fuse: new Fuse(response.data.doctor.patients, options) } );
  
			} )
			.catch((error) => {
				console.log(error);
			})
	}

  setPatientView(patients){
    this.setState({ patients: patients })
  }
  
  render(){

		const patients = this.state.patients.map((patient, index) =>
			<Link key={index} 
      to={{ pathname: '/patients',  
        state: { patientId: this.state.patients[index].patient_id,
                 doctorId: this.props.id
              }     
        }} 
				className="collection-item center-align"> { patient.patientName }</Link>
			)

		return(
		  <div>
        <NavigationBar search= {true} fuse= { this.state.fuse} patientList={ this.state.patientList } setPatientView ={ this.setPatientView } ></NavigationBar>
        <div className="container">
        <div className="collection with-header">
          <div className="collection-header center-align">
            <h5>Patients</h5>
          </div>
          { patients }
        </div>

        </div>
		  </div>
			)
	}
}
