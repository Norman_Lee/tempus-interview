import React from 'react';
import ReactDOM from 'react-dom';
import DoctorPatientView from './DoctorPatientView.jsx';
import PatientView from './PatientView.jsx';
import LoginView from './LoginView.jsx';
import SearchView from './SearchView.jsx';
import { Router, Route } from 'react-router-dom';
import Auth from './auth0/Auth.js';
import Authorization from './auth0/Authorization.jsx';
import Callback from './auth0/Callback.jsx';
import createBrowserHistory from 'history/createBrowserHistory';
import NavigationBar from './components/NavigationBar.jsx';


export default class App extends React.Component {
	constructor(props){
		super(props);
		this.history = createBrowserHistory();
		this.auth = new Auth(this.history);

	}		

	render(){

		const Doctor = Authorization(['doctor']);
		const Patient = Authorization(['patient']);

		return(
			<Router history= { this.history }>
      <div> 
        <Route exact path="/login" render={ routeProps => <LoginView {...routeProps} auth={this.auth}/>}></Route>
        <Route exact path="/" component={Doctor(SearchView,this.auth)}></Route>
        <Route exact path="/patients" component={Doctor(DoctorPatientView,this.auth)}></Route>
        <Route exact path="/" component={Patient(PatientView,this.auth)}></Route>
        <Route path="/callback" render={ routeProps => <Callback {...routeProps} auth={this.auth}/> } />
      </div>
      </Router>
		)
	}
}

