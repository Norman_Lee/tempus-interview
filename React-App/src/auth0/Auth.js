import auth0 from 'auth0-js';

export default class Auth {
	
	constructor(history){	
		this.auth0 = new auth0.WebAuth({
	    	domain: 'norman-lee.auth0.com',
	    	clientID: 'qaGqmLbEgX4tUMfkrKJjWmdtNZHXr7Zp',
	    	redirectUri: 'http://localhost:8080/callback',
	    	audience: 'tempus-interview',
	    	responseType: 'token id_token',
	    	scope: 'openid profile'
	  	});

	  this.login = this.login.bind(this);
	  this.handleAuthentication = this.handleAuthentication.bind(this);
	  this.setSession = this.setSession.bind(this);
	  this.logout = this.logout.bind(this);
	  this.isAuthenticated = this.isAuthenticated.bind(this);
	  this.history = history;
}

  	login() {
  		console.log("Auth login was trggered");
    	this.auth0.authorize();
  	}

  	handleAuthentication(){
  		this.auth0.parseHash((err, authResult) => {
  			if(authResult && authResult.accessToken && authResult.idToken){
  				console.log("Am I here?")
  				this.setSession(authResult);
  				this.history.replace('/');
  			} else if(err) {
  				console.log("Am I here in err")
  				this.history.replace('/');
  				console.log(err);
  			}
  		});
  	}

  	setSession(authResult){
  		let expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
      localStorage.setItem('access_token', authResult.accessToken);
      localStorage.setItem('id_token', authResult.idToken);
  		localStorage.setItem('expires_at', expiresAt);
      
      this.history.replace('/');
  	}

  	logout(){
  		localStorage.removeItem('access_token');
  		localStorage.removeItem('id_token');
  		localStorage.removeItem('expires_at');

  		this.history.replace('/');
  	}

  	isAuthenticated(){
  		let expiresAt = JSON.parse(localStorage.getItem('expires_at'));
      return new Date().getTime() < expiresAt;
  	}
}
