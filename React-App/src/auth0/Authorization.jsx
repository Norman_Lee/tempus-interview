import React from 'react';
import { Redirect } from 'react-router'
import axios from 'axios'

const Authorization = (allowedRoles) => (WrappedComponent, auth) => {
	return class WithAuthorization extends React.Component {
		constructor(props){
			super(props);					
		
			this.state = {
				user: {
					role: "",
					id: ""
				}
			}
		}

		shouldComponentUpdate(nextProps, nextState){
			if(nextState.user.role != this.state.user.role){
				return true;
			}
			return false;
		}

    componentDidMount(){
      let token = localStorage.getItem('access_token');
      axios.get('http://localhost:3000/role', { headers: {'Authorization' : 'Bearer ' + token}})
				.then((response) => {
					console.log(response.data);
					this.setState(response.data);
				}) 
				.catch((error) => {
					console.log(error);
				})
		}

		componentDidUpdate(){

		}		
		
		render(){
			const { role } = this.state.user;
			if(auth.isAuthenticated()){
				if(allowedRoles.includes(role)){
					return <WrappedComponent {...this.props } id={ this.state.user.id } auth= { auth } />
				} else {
					return null;
				}
			} else {
				console.log("Got redirected nooo")
				return <Redirect to="/login"/>
			}
		}
	}
}

export default Authorization;
