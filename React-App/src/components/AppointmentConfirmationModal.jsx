import React from 'react';
import axios from 'axios';
import Modal from 'react-responsive-modal';
import Datetime from 'react-datetime'; 
import Button from './Button.jsx';
import moment from 'moment';

export default class AppointmentConfirmationModal extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {
      datetime: moment(this.props.appointment.date),
      selectedOption: 15,
      error: false
    }
    
    this.closeModal = this.closeModal.bind(this)
    this.handleDateTimeChange = this.handleDateTimeChange.bind(this);
    this.handleDurationChange = this.handleDurationChange.bind(this);
    this.isValidDate = this.isValidDate.bind(this);
    this.handleError = this.handleError.bind(this);
    this.updateAppointment = this.updateAppointment.bind(this); 
  }


  closeModal(){
    this.setState({ error: false })
    this.props.closeAppointmentConfirmationModal();
  }

  handleDateTimeChange(m){
    this.setState({ datetime: m });
  }
  
  updateAppointment(){
    let updatedAppointment = this.props.appointment;
    console.log(updatedAppointment) 
    updatedAppointment.date = this.state.datetime.format("YYYY-MM-DD")
    updatedAppointment.time = this.state.datetime.format("hh:mm A")
    this.props.approveAppointment(updatedAppointment, this.props.index);
    this.closeModal()  
  }

  handleDurationChange(event){
    this.setState({ selectedOption: Number(event.target.value) })
  }

  isValidDate(current){
    let yesterday = Datetime.moment().subtract(1, 'day');
    return current.isAfter(yesterday);
  }
  
  handleError(){
    this.setState({ error: true})
  }
  render(){

    let errorMessage = null;
    if(this.state.error){
     errorMessage =  <div> Appointment update failed!</div>
    }
    
    return(
    <Modal 
      open= { this.props.isOpen }
      onClose = {this.closeModal}
      closeOnOverlayClick = {false}
      little = { true }  
    > 
    { errorMessage }
    <form> 
      <label>Appointment</label>
      <input id="appointment" type="text" value={ this.props.appointment.description } disabled/> 
    
      <label>Date</label>
      <Datetime inputProps= { { placeholder: "10/10/1000", required: true } } timeConstraints={ { hours:{ min: 9, max: 18 }, minutes: { step: 15 } } } isValidDate ={ this.isValidDate }  value={ this.state.datetime } onChange= { this.handleDateTimeChange }/>
      
      <p>   
        <input name="duration" type="radio" id="1" value={15} checked= { this.state.selectedOption === 15 } onChange= { this.handleDurationChange } />
        <label htmlFor="1">15 Minutes</label>
      </p>
      <p> 
        <input name="duration" type="radio" id="2" value={30} checked= { this.state.selectedOption === 30 } onChange= { this.handleDurationChange } />
        <label htmlFor="2">30 Minutes</label>
      </p>
      <p>
        <input name="duration" type="radio" id="3" value={60} checked= { this.state.selectedOption === 60 } onChange= { this.handleDurationChange } />
        <label htmlFor="3">1 Hour</label>
        </p>
    </form>
    <Button name={ "Confirm" } style= { "margin-increase" } handler={ this.updateAppointment }></Button>
    <Button name={ "Cancel" } style= { "margin-increase" } handler={ this.props.closeAppointmentConfirmationModal }></Button>
    </Modal>
    )
  }
}
