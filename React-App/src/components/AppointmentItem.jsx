import React from 'react';

export default class AppointmentItem extends React.Component {
	render(){
		return (
			<div className="collection-item">
				<span>
					<div>
					{ this.props.appointment.description }
					</div>
					<div>
						{ this.props.appointment.date } {this.props.appointment.time }
					</div>
				</span>
			</div>
			)
	}	
}