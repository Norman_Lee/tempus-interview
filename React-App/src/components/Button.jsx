import React from 'react';
import axios from 'axios';
import { Button } from 'react-materialize';

export default class NewAppointmentButton extends React.Component {
	constructor(props){
		super(props);
	}

	render(){
		return(
      <Button waves="light" className= { "max-size " + this.props.style } onClick={this.props.handler}>
      { this.props.name }
			</Button>);
	}
}
