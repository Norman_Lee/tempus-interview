import React from 'react';
import axios from 'axios';

export default class CancelableAppointmentItem extends React.Component {
  constructor(props){
    super(props); 
  }

  render(){
		return (
			<div className="row collection-item">
				<div className="col l4">
				{ this.props.appointment.description }
				</div>
				<div className="col l4">
					{ this.props.appointment.date } {this.props.appointment.time }
				</div>
        <a className="col l4 right-align" >
					<i className="small material-icons pointer" onClick={() => { this.props.cancelAppointment(this.props.appointment._id, "future", this.props.index)} }>highlight_off</i>
				</a>
			</div>
			)
	}	
}
