import React from 'react';
import axios from 'axios';
import Modal from 'react-responsive-modal';

export default class ImageViewModal extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {
      error: false
    }
    
    this.closeModal = this.closeModal.bind(this)
  }
  
  closeModal(){
    this.setState({ error: false })
    this.props.closeImageViewModal();
  }

  render(){

    let errorMessage = null;
    if(this.state.error){
     errorMessage =  <div> Record display failed!</div>
    }
    
    let display = <img width="700" src={"data:image;base64," + this.props.image }></img>
    if(this.props.mimetype === 'application/pdf'){
      display = <iframe width="700" height="900" src={"data:application/pdf;base64," + this.props.image }></iframe>
    }
    
    return(
    <Modal 
      open= { this.props.isOpen }
      onClose = {this.closeModal}
      closeOnOverlayClick = { true }
      little = { true } 
      showCloseIcon = {false}
    > 
    { errorMessage }
    { display }
    </Modal>
    )
  }
}
