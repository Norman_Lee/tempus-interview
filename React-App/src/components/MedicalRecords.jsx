import React from 'react';
import NewMedicalRecordModal from './NewMedicalRecordModal.jsx';
import ImageViewModal from './ImageViewModal.jsx';
import { Collection, CollectionItem } from 'react-materialize';
import axios from 'axios';
import NewMedicalRecordButton from './NewMedicalRecordButton.jsx';

export default class MedicalRecords extends React.Component {
  constructor(props){
    super(props);
    //console.log(this.props.medicalRecords);
    this.state = {
      medicalRecords: this.props.medicalRecords,
      
      imageViewModalOpen: false,
      image: '',
      mimetype: '' 
    
    }
    this.deleteMedicalRecord = this.deleteMedicalRecord.bind(this);
    this.openImageViewModal = this.openImageViewModal.bind(this);
    this.closeImageViewModal = this.closeImageViewModal.bind(this);
    this.createNewMedicalRecord = this.createNewMedicalRecord.bind(this);
  }
  
  

  deleteMedicalRecord(fileId,index){
    let token = localStorage.getItem('access_token');
    axios.delete('http://localhost:3000/doctors/'+ this.props.doctorId +'/patients/' + this.props.patientId + '/medicalrecords/' + fileId , 
      { headers: {'Authorization' : 'Bearer ' + token}})
			.then((response) => {
				//Do some conversion to get it into correct data state
        console.log("Does it get here?");
        let newMedicalRecords  = this.state.medicalRecords;
        newMedicalRecords.splice(index, 1);
        this.setState({ medicalRecords: newMedicalRecords  });
        console.log("Past here?")
			} )
			.catch((error) => {
			})
    
  }
  
  openImageViewModal(fileId){
    let token = localStorage.getItem('access_token')      
    axios.get('http://localhost:3000/doctors/'+ this.props.doctorId +'/patients/' + this.props.patientId + '/medicalrecords/' + fileId, { headers: {'Authorization' : 'Bearer ' + token}})
			.then((response) => {
				//Do some conversion to get it into correct data state
        console.log(response);
        this.setState({ imageViewModalOpen: true, image: response.data.image, mimetype: response.data.mimetype }) 
      })
			.catch((error) => {
				console.log(error);
        this.setState({ imageViewModalOpen: true }) 
      })


  }
  
  createNewMedicalRecord(formData, handleError, handleSuccess){
    let token = localStorage.getItem('access_token')      
    axios.post('http://localhost:3000/doctors/' + this.props.doctorId + '/patients/' + this.props.patientId + '/medicalRecords', formData
      , { headers: {'Authorization' : 'Bearer ' + token, 
        'Content-Type' : 'multipart/form-data'}}) 
      .then((response) => { 
        console.log(response);

        let medicalRecords = this.state.medicalRecords;

        medicalRecords.push(response.data);

        this.setState({ medicalRecords: medicalRecords })
        handleSuccess();
			})	
			.catch((error) =>{
				console.log(error);
        handleError();
      })
  
  }
  closeImageViewModal(){
    this.setState({ imageViewModalOpen: false}) 
  }
  
  componentWillReceiveProps(nextProps){
    console.log("Printin out medicalRecords")
    console.log(this.props.medicalRecords);
    this.setState({ medicalRecords: nextProps.medicalRecords })
  }
  
  

  componentDidMount(){
    console.log("Mounted")
  }

  render(){

    let deleteMedicalRecordButton = null
    let newMedicalRecordButton = <li className="collection-header center-align"><h4>Medical Records</h4></li>

    let medicalRecords = this.state.medicalRecords.map((record, index) => 
      <li className="collection-item center-align" key= { index } > 
      { record.name }
				<div className="secondary-content"><i className="small material-icons pointer">system_update_alt</i></div>
        <div className="secondary-content"><i className="small material-icons pointer" onClick={ () =>{ this.openImageViewModal(record.fileId) } }>info_outline</i></div>
      </li>)
      

    if(this.props.doctorId){ 
      
      medicalRecords = this.state.medicalRecords.map((record, index) => 
      <li className="collection-item center-align" key= { index } > 
      { record.name }
      <div className="secondary-content"><i className="small material-icons pointer" onClick={ () => { this.deleteMedicalRecord(record.fileId, index) } }>highlight_off</i></div>
      <div className="secondary-content"><i className="small material-icons pointer">system_update_alt</i></div>
        <div className="secondary-content"><i className="small material-icons pointer" onClick={ () =>{ this.openImageViewModal(record.fileId) } }>info_outline</i></div>
        </li>
      )
        
        newMedicalRecordButton = <NewMedicalRecordButton doctorId= { this.props.doctorId } patientId={ this.props.patientId } createNewMedicalRecord={ this.createNewMedicalRecord} ></NewMedicalRecordButton>
    }
    


    return(
      <div>
      <ul className="collection with-header">
      { newMedicalRecordButton }
      { medicalRecords }
      </ul>
          <ImageViewModal isOpen={ this.state.imageViewModalOpen } mimetype={ this.state.mimetype } image= { this.state.image } closeImageViewModal={this.closeImageViewModal}></ImageViewModal>
      </div>
			)
	}
}
