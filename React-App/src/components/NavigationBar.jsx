import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Navbar, NavItem } from 'react-materialize';

export default class NavigationBar extends React.Component{
 	constructor(props){
    super(props);
    this.handleInput = this.handleInput.bind(this);
  }

  componentDidMount(){
  
  }
    
  handleInput(event){
    console.log(event.target.value)
    let result = this.props.fuse.search(event.target.value)
    if(event.target.value === ''){
      result = this.props.patientList
    }
    this.props.setPatientView(result);
  }

  render(){
    
    const hasSearchLink = this.props.searchLink;
    const hasSearch = this.props.search;
    
    let navigationlinks = null;
    let searchbar = null;
    let logo = null;
    let buttoncollapse = null
    let sidenav = null
    
    if(hasSearchLink){
      navigationlinks = <li><Link to='/'>Search</Link></li>
    }
    
    if(hasSearch){
      searchbar = (
        <form>
          <div className="input-field">
            <input id="search" type="search" onChange={ this.handleInput } />
            <label className="label-icon" htmlFor="search"><i className="material-icons">search</i></label>
            <i className="material-icons">close</i>
          </div>
         </form>
      )
    }

    return(
      <Navbar brand={ ( hasSearchLink || hasSearch) ? null:"logo" } className="blue" left={ hasSearchLink }>
      { navigationlinks }
      { searchbar }
      </Navbar>
    )
  }
}
