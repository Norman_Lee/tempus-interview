import React from 'react';
import axios from 'axios';
import Button from './Button.jsx';
import NewAppointmentModal from './NewAppointmentModal.jsx';

export default class NewAppointmentButton extends React.Component {
	constructor(props){
    super(props);

    this.state = {
      newAppointmentModalOpen: false
    }
    
    this.closeNewAppointmentModal = this.closeNewAppointmentModal.bind(this);
    this.openNewAppointmentModal = this.openNewAppointmentModal.bind(this);
	}

  openNewAppointmentModal(){
    this.setState({ newAppointmentModalOpen: true})
  }
  
  closeNewAppointmentModal(){ 
    this.setState({ newAppointmentModalOpen: false})
  }
	render(){
		return(
      <div>
        <Button name= { "New Appointment" } handler={ this.openNewAppointmentModal }></Button> 
        <NewAppointmentModal createNewAppointment= { this.props.createNewAppointment } isOpen={ this.state.newAppointmentModalOpen } closeNewAppointmentModal={this.closeNewAppointmentModal}></NewAppointmentModal>
      </div>
      );
	}
}
