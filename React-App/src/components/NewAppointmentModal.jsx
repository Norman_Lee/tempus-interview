import React from 'react';
import axios from 'axios';
import Modal from 'react-responsive-modal';
import Datetime from 'react-datetime'; 
import Button from './Button.jsx';
import moment from 'moment';

export default class NewAppointmentModal extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {
      appointmentName: '',
      datetime: moment({ hour: 9}),
      selectedOption: 15,
      error: false
    }
    
    this.closeModal = this.closeModal.bind(this)
    this.handleAppointmentNameChange = this.handleAppointmentNameChange.bind(this);
    this.handleDateTimeChange = this.handleDateTimeChange.bind(this);
    this.handleDurationChange = this.handleDurationChange.bind(this);
    this.createAppointment = this.createAppointment.bind(this);
    this.isValidDate = this.isValidDate.bind(this);
    this.handleError = this.handleError.bind(this);
  }


  closeModal(){
    this.setState({ error: false })
    this.props.closeNewAppointmentModal();
  }

  handleAppointmentNameChange(event){
    this.setState({ appointmentName: event.target.value });
  }
  
  createAppointment(){
    this.props.createNewAppointment(this.state.datetime.toString(),this.state.appointmentName, this.handleError); 
    this.props.closeNewAppointmentModal();
  }
  
  handleDateTimeChange(m){
    this.setState({ datetime: m });
  }

  handleDurationChange(event){
    this.setState({ selectedOption: Number(event.target.value) })
  }

  isValidDate(current){
    let yesterday = Datetime.moment().subtract(1, 'day');
    return current.isAfter(yesterday);
  }
  
  handleError(){
    this.setState({ error: true})
  }
  render(){

    let errorMessage = null;
    if(this.state.error){
     errorMessage =  <div> Appointment creation failed!</div>
    }
    
    return(
    <Modal 
      open= { this.props.isOpen }
      onClose = {this.closeModal}
      closeOnOverlayClick = {false}
      little = { true }  
    > 
    { errorMessage }
    <form> 
      <label>Appointment</label>
      <input placeholder="Appointment" id="appointment" type="text" value={ this.state.appointmentName } onChange={ this.handleAppointmentNameChange } required/> 
    
      <label>Date</label>
      <Datetime inputProps= { { placeholder: "10/10/1000", required: true } } timeConstraints={ { hours:{ min: 9, max: 18 }, minutes: { step: 15 } } } isValidDate ={ this.isValidDate }  value={ this.state.datetime } onChange= { this.handleDateTimeChange }/>
      
      <p>   
        <input name="duration" type="radio" id="1" value={15} checked= { this.state.selectedOption === 15 } onChange= { this.handleDurationChange } />
        <label htmlFor="1">15 Minutes</label>
      </p>
      <p> 
        <input name="duration" type="radio" id="2" value={30} checked= { this.state.selectedOption === 30 } onChange= { this.handleDurationChange } />
        <label htmlFor="2">30 Minutes</label>
      </p>
      <p>
        <input name="duration" type="radio" id="3" value={60} checked= { this.state.selectedOption === 60 } onChange= { this.handleDurationChange } />
        <label htmlFor="3">1 Hour</label>
        </p>
    </form>
    <Button name={ "Create" } style= { "margin-increase" } handler={ this.createAppointment }></Button>
    <Button name={ "Cancel" } style= { "margin-increase" } handler={ this.props.closeNewAppointmentModal }></Button>
    </Modal>
    )
  }
}
