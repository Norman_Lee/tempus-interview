import React from 'react';
import axios from 'axios';
import Button from './Button.jsx';
import NewMedicalRecordModal from './NewMedicalRecordModal.jsx';

export default class NewAppointmentButton extends React.Component {
	constructor(props){
    super(props);

    this.state = {
      newMedicalRecordModalOpen: false
    }
    
    this.openNewMedicalRecordModal = this.openNewMedicalRecordModal.bind(this);
    this.closeNewMedicalRecordModal = this.closeNewMedicalRecordModal.bind(this); 
    this.createNewMedicalRecord = this.createNewMedicalRecord.bind(this);
	}

  
  openNewMedicalRecordModal(){
    this.setState({ newMedicalRecordModalOpen: true})
  }
  
  closeNewMedicalRecordModal(){ 
    this.setState({ newMedicalRecordModalOpen: false})
  }

  createNewMedicalRecord(formData, handleError){
    this.props.createNewMedicalRecord(formData, handleError, this.closeNewMedicalRecordModal);
  }
	render(){
		return(
      <div>
        <Button name={ "Upload New Medical Record" } handler={ this.openNewMedicalRecordModal}></Button>
          <NewMedicalRecordModal createNewMedicalRecord ={ this.createNewMedicalRecord } isOpen= { this.state.newMedicalRecordModalOpen } closeNewMedicalRecordModal= {this.closeNewMedicalRecordModal}></NewMedicalRecordModal>
      </div>
      );
	}
}
