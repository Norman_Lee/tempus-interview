import React from 'react';
import axios from 'axios';
import Modal from 'react-responsive-modal';
import Button from './Button.jsx';

export default class NewMedicalRecordModal extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {
      medicalRecordName: '',
      formData: '',
      error: false
    }
    
    this.closeModal = this.closeModal.bind(this)
    this.handleMedicalRecordNameChange = this.handleMedicalRecordNameChange.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.createMedicalRecord = this.createMedicalRecord.bind(this);
    this.handleError = this.handleError.bind(this);
  }


  closeModal(){
    this.setState({ error: false })
    this.props.closeNewMedicalRecordModal();
  }

  handleMedicalRecordNameChange(event){
    this.setState({ medicalRecordName: event.target.value });
  }
  
  createMedicalRecord(){
    let data = this.state.formData
    data.append('name', this.state.medicalRecordName)
    this.props.createNewMedicalRecord(data, this.handleError);
  }

  handleFileChange(event){
    console.log(event.target.files[0]);
    let data = new FormData();
    data.append('file', event.target.files[0])
    this.setState({ formData: data }) 
  }

  handleError(){
    this.setState({ error: true})
  }

  render(){
     
    let errorMessage = null;
    if(this.state.error){
     errorMessage =  <div> Medical Record creation failed!</div>
    }
    
    return(
    <Modal 
      open= { this.props.isOpen }
      onClose = {this.closeModal}
      closeOnOverlayClick = {false}
      little = { true } 
       modalStyle= { { width: '35%'} }
    >  
    { errorMessage }
    <form> 
      <label>MedicalRecord</label>
      <input placeholder="MedicalRecord" type="text" value={ this.state.appointmentName } onChange={ this.handleMedicalRecordNameChange } required/> 
    
      <div className="file-field input-field">
        <div className="btn">
          <span>File</span>
          <input type="file" onChange={ this.handleFileChange }/>
        </div>
        <div className="file-path-wrapper">
          <input className="file-path" type="text"/>
        </div>
      </div>
    </form>
    <Button name={ "Create" } style= { "margin-increase" } handler={ this.createMedicalRecord }></Button>
    <Button name={ "Cancel" } style= { "margin-increase" } handler={ this.props.closeNewMedicalRecordModal }></Button>
    </Modal>
    )
  }
}
