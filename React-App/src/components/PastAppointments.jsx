import React from 'react';
import { Collection } from 'react-materialize';
import AppointmentItem from './AppointmentItem.jsx';


export default class PastAppointments extends React.Component {
  constructor(props){
    super(props);
  
  }
  
  render(){

    const pastAppointments = this.props.pastAppointments.map((appointment, index) =>
			<AppointmentItem key={ index } appointment={appointment}/>
			) 
    
    return (
				<Collection header="Past">
					{ pastAppointments }
				</Collection>
			)
	}
}
