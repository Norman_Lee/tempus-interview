import React from 'react';
import {Row, Col} from 'react-materialize'

export default class PatientBanner extends React.Component {
	render(){
		return(
				<Row className="card-panel grey lighten-4 z-depth-1 valign-wrapper">
					<Col l={2}>
						<img className="circle responsive-img" src= {this.props.patient.profileUrl} alt={this.props.patient.name}></img>
					</Col>
					<Col l={5} className="center-align"> <h4>{ this.props.patient.name }</h4></Col>
					<Col l={5} className="center-align"> <h5>{ this.props.patient.age }</h5></Col>
				</Row>
			)
	}
}
