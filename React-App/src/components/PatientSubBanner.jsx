import React from 'react';
import {Row, Col} from 'react-materialize'

export default class PatientSubBanner extends React.Component {
	render(){
		return(
			<Row className="grey card horizontal lighten-4">
				<Col l={4} className="center-align">{ this.props.patient.mailingAddress }</Col>
				<Col l={4} className="center-align">{ this.props.patient.phoneNumber }</Col>
				<Col l={4} className="centrer-align">{ this.props.patient.emailAddress }</Col>
			</Row>
			)
	}
}
