import React from 'react';
import { Collection } from 'react-materialize';
import NewAppointmentButton from './NewAppointmentButton.jsx';
import ApprovableAppointmentItem from './ApprovableAppointmentItem.jsx';
import AppointmentConfirmationModal from './AppointmentConfirmationModal.jsx';
import CancelableAppointmentItem from './CancelableAppointmentItem.jsx';


export default class PendingAppointments extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {

      appointmentConfirmationModalOpen: false,
      currentAppointment: '',
      currentAppointmentIndex: ''
    }
    
    this.openAppointmentConfirmationModal = this.openAppointmentConfirmationModal.bind(this); 
    this.closeAppointmentConfirmationModal = this.closeAppointmentConfirmationModal.bind(this); 
  }
  
  
  
  openAppointmentConfirmationModal(appointment, index){
    this.setState({ 
      appointmentConfirmationModalOpen: true, 
      currentAppointment: appointment,
      currentAppointmentIndex: index
    })
  }
  
  closeAppointmentConfirmationModal(){  
    this.setState({ appointmentConfirmationModalOpen: false}) 
  }
  render(){
    
   let pendingAppointments = this.props.pendingAppointments.map((appointment, index) =>
      <CancelableAppointmentItem key={ index } appointment={appointment} cancelAppointment={ this.props.cancelAppointment} index={ index } />)
   
    let newAppointmentButton = <div></div> 

    if(!this.props.patient){ 
      newAppointmentButton = <NewAppointmentButton createNewAppointment = { this.props.createNewAppointment } ></NewAppointmentButton>
     pendingAppointments = this.props.pendingAppointments.map((appointment, index) =>
      <ApprovableAppointmentItem key={ index } appointment={appointment} cancelAppointment={ this.props.cancelAppointment} openModal= { this.openAppointmentConfirmationModal } index={ index } />
			)
    }

    return (
      
      <div>
        <Collection header="Pending">
        { pendingAppointments }
        { newAppointmentButton }
        </Collection>
        
        <AppointmentConfirmationModal isOpen={ this.state.appointmentConfirmationModalOpen } appointment= { this.state.currentAppointment } index= { this.state.currentAppointmentIndex }  approveAppointment= { this.props.approveAppointment } closeAppointmentConfirmationModal= { this.closeAppointmentConfirmationModal }  ></AppointmentConfirmationModal>
      </div>  
      )
	}
}
