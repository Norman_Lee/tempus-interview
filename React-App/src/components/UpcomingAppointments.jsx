import React from 'react';
import { Collection } from 'react-materialize';
import CancelableAppointmentItem from './CancelableAppointmentItem.jsx';


export default class UpcomingAppointments extends React.Component {
  constructor(props){
    super(props);
  
  }
  
  render(){
    
    const futureAppointments = this.props.futureAppointments.map((appointment, index) =>
	<CancelableAppointmentItem key={ index } appointment={appointment} cancelAppointment={ this.props.cancelAppointment} index={ index } />
			)
    
    return (
				<Collection header="Upcoming">
					{ futureAppointments }
				</Collection>
			)
	}
}
