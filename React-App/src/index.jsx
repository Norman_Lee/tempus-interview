import React from 'react';
import ReactDOM from 'react-dom';
import App from './app.jsx';

import '../node_modules/react-datetime/css/react-datetime.css'

import './scss/app.scss'

ReactDOM.render(
	<App/>,
	document.getElementById('root')
)
