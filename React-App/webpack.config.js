const path = require('path');
const webpack = require('webpack');

const config = {
	entry: './src/index.jsx',
	
	plugins: [
		new webpack.HotModuleReplacementPlugin() //Enable HMR
	],

	output: {
		path: path.resolve(__dirname, 'dist/'),
		filename: 'bundle.js',
		publicPath: '/'
	},
	// watch: true,
	devServer: {
		// hot: true,
		publicPath: '/',
		contentBase: path.resolve(__dirname, 'public'),
		watchOptions:{
			poll:true
		},
		historyApiFallback: true
	},
	module: {
		rules : [
			{ test: /\.(js|jsx)$/, exclude: /node_modules/, use: 'babel-loader'},
			{ test: /\.scss$/, use: [ 'style-loader', 'css-loader', 'sass-loader']}, 
			{ test: /\.css$/, use: [ 'style-loader', 'css-loader']},
      { test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*$|$)/, use: 'file-loader'}			
		]
	}

}

module.exports = config;
