const MongoClient = require('mongodb').MongoClient;
const moment = require('moment');

const url = 'mongodb://localhost:27017/tempus-interview';

var mongoDatabase;
var patients;

const ISODate = require('mongodb').ISODate;

MongoClient.connect(url, function(err,db){
   if(err)throw err;

   mongoDatabase = db;
  
   patients = mongoDatabase.collection('patients');

    patients.update(
      {  },
      { $pull: { "appointments": { appointmentType: ['future', 'pending'], date: { $lt: ISODate(moment().format('YYYY-MM-DD') } } } },
      { multi: true },
      (err, result) => {
        console.log(result);
        res.status(200).send('Did it fam');
    })

}
