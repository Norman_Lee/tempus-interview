const express = require('express');
const app = express();
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const moment = require('moment');
const bodyParser = require('body-parser');
const validate = require('express-validation');
const appointmentCreation = require('./validation/appointmentCreation.js');
const multer = require('multer');
const upload = multer({ dest: 'medicalrecords/'});
const path = require('path');
const fs = require('fs');

const ObjectID = require('mongodb').ObjectID;

const patientRouter = express.Router();
const doctorRouter = express.Router();

const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017/tempus-interview';

var mongoDatabase;
var patients;
var doctors;



//Enabling CORS
app.use(function(req, res, next){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
	next();
})


app.use(jwt({

  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: 'https://norman-lee.auth0.com/.well-known/jwks.json'
  }),

  audience: 'tempus-interview',	
  issuer: 'https://norman-lee.auth0.com/',
  algorithms: [ 'RS256' ]
  })
)

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

patientRouter.use('/:patientId*' ,(req, res, next) => {

  if(req.method == 'OPTIONS'){
    next(); 
  } else {
    const role = req.user['http://localhost/roles'];
    const id = req.user['http://localhost/id'];
    const invalidDoctorPatient = false; //Need to find proper check here, if not under care

    if(req.user.scope.includes('read:patients') && role === 'patient' && id == req.params.patientId){
      next()
    } else if(req.user.scope.includes('read:patients') && role === 'doctor'){
      
      patients.find({ patient_id: Number(req.params.patientId) }).toArray((err, docs) => {
        if(err || docs.length === 0) res.status(500).end();
        
        if(docs[0].doctors.includes(id)){    
          next();
        } else {
          res.status(403).end();
        }

      })  
    } else {
      res.status(403).send({ message: 'Forbidden' })
    }
  }
})


//Patients
patientRouter.get('/:patientId', function(req, res){
  patients.find({ patient_id: Number(req.params.patientId) }).toArray((err, docs) => {
    if(err) res.status(500).end();
    delete docs[0].appointments;

    res.status(200).send({	
      patient: docs[0]
    })

  })  
})

patientRouter.get('/:patientId/appointments', function(req,res){	
  if(!req.user.scope.includes('read:appointments')){
    res.status(403).send({ message: 'Forbidden' })
  } else { 
    patients.find({ patient_id: Number(req.params.patientId) }, { appointments: true }).toArray((err, docs) => {
      if(err || docs.length === 0) res.status(500).end();
      
      let appointments = { past: [], pending: [], future: []}
      //Should I do a reduce call?
      let transformDate = (appointment, index) => {
        let date = moment(appointment.date).format("YYYY-MM-DD");
        let time = moment(appointment.date).format("hh:mm A");

        appointment.date = date;
        appointment.time = time;

        if(appointment.appointmentType === "past"){
          appointments.past.push(appointment);
        } else if (appointment.appointmentType === "pending"){
          appointments.pending.push(appointment);
        } else if (appointment.appointmentType === "future"){
          appointments.future.push(appointment); 
        }
      }
      
      docs[0].appointments.map(transformDate);

      res.status(200).send({	
        appointments: appointments
      })
    })  
  }
})


//Appointments
patientRouter.post('/:patientId/appointments', validate(appointmentCreation), function(req, res){
	//Validate that body is correct
	//Probably add a unique id generator
  //Add validation to check if this is part of a doctor or not
  if(!req.user.scope.includes('create:appointments')){
    res.status(403).send({ message: 'Forbidden' })
  } else {
    console.log("Is creating yay");
    let newAppointment = req.body;
    newAppointment._id = new ObjectID();

    patients.updateOne(
      { patient_id: Number(req.params.patientId)},
      { $push: { "appointments": newAppointment }}, (err, result) => {
        if(err){ 
          res.status(500).end();
        } else {
          res.status(201).end();
        }
    });
  }
})

patientRouter.delete('/:patientId/appointments/:appointmentId', function(req, res){
	//Handle race conditions with doctor and patient trying to post and remove things 
	
  if(!req.user.scope.includes('delete:appointments')){
    res.status(403).send({ message: 'Forbidden' })
  } else {
    console.log(req.params.appointmentId);
    console.log("Is deleting yay");
    

    patients.updateOne(
      { patient_id: Number(req.params.patientId) },
      { $pull: { "appointments": {_id: ObjectID(req.params.appointmentId) } } },
      (err, result) => {
        console.log(result);
        res.status(200).send('Did it fam');
    })
  }
})

patientRouter.put('/:patientId/appointments/:appointmentId', function(req, res){
  if(!req.user.scope.includes('update:appointments')){
    res.status(403).send({ message: 'Forbidden' })
  } else if( moment(req.body.date).isBefore(moment().format('YYYY-MM-DD')) || !moment(req.body.date).isBetween( '9:00 AM', '6:00 PM', 'hour' ,[]) ) {
    res.status(400).send({ message: 'Invalid date' })
  } else {  
    console.log("Is updating yay");
    patients.updateOne(
      { patient_id: Number(req.params.patientId), "appointments._id": ObjectID(req.params.appointmentId) },
      { $set: { "appointments.$": req.body }}, (err, result) => {
        if(err) res.status(500).end();
        
        res.status(200).end();
        console.log("Did it non-atomically fam");
    });
  }
})


patientRouter.use('/:patientId/medicalrecords/:fileId', (req,res,next) => {  
  patients.find({ patient_id: Number(req.params.patientId)},{ medicalRecords: { $elemMatch: { fileId: req.params.fileId } } }).toArray((err, docs) => {
    if(err || docs.length === 0){
      res.status(403).end();
    } else {
      next();
    }
  })  
})

patientRouter.get('/:patientId/medicalrecords/:fileId', (req,res,next) => {
  if(!req.user.scope.includes('read:medicalrecords')){
    res.status(403).send({ message: 'Forbidden' })
  } else {
    let file = fs.readFileSync(path.join(__dirname + '/medicalrecords/' + req.params.fileId));
    let buffer = new Buffer(file).toString('base64')
    
  patients.find({ patient_id: Number(req.params.patientId) }, { medicalRecords: { $elemMatch: { fileId: req.params.fileId } } } ).toArray((err, docs) => {
    if(err || docs.length == 0){
      res.status(500).end()
    } else{
      console.log(docs[0].medicalRecords[0].mimetype)
      res.status(200).send({ image: buffer, mimetype: docs[0].medicalRecords[0].mimetype })
    }
  })
  }
});

//Create a medical record
patientRouter.post('/:patientId/medicalrecords', upload.single('file'), function(req, res){
  if(!req.user.scope.includes('create:medicalrecords')){
    res.status(403).send({ message: 'Forbidden' })
  } else {
    let newMedicalRecord = { name: req.body.name, fileId: req.file.filename, mimetype: req.file.mimetype }
    patients.updateOne(
      { patient_id: Number(req.params.patientId)},
      { $push: { "medicalRecords": newMedicalRecord }}, (err, result) => {
        if(err) {
          res.status(500).end();
        } else {
          res.status(201).send(newMedicalRecord);
        }
    });
  }
})


patientRouter.delete('/:patientId/medicalrecords/:fileId', function(req, res){
  if(!req.user.scope.includes('delete:medicalrecords')){
    res.status(403).send({ message: 'Forbidden' })
  } else {
    patients.updateOne(
      { patient_id: Number(req.params.patientId) },
      { $pull: { "medicalRecords": { fileId: req.params.fileId } } },
      (err, result) => {
        if(err){ 
          res.status(500).end();
        } else {
          fs.unlink(path.join(__dirname + '/medicalrecords' + '/' + req.params.fileId ), (err) => {
            if(err){
              console.log(err); 
              console.log("Did it get here instead?");
              res.status(500).end();
            } else {
              res.status(200).end();
            }
          })
        }
    })
  }
})



doctorRouter.use('/:doctorId*',( req, res, next ) => {
  if(req.method == 'OPTIONS'){
    next();
  } else {
    const role = req.user['http://localhost/roles'];
    const id = req.user['http://localhost/id'];
    
    if(req.user.scope.includes('read:patients') && role  === 'doctor' && id == req.params.doctorId){
      next();
    } else {
      res.status(403).send({ message: 'Forbidden' }) 
    }
  }
})
//Doctors
doctorRouter.get('/:doctorId/patients', function(req, res){
    doctors.find({ doctor_id: Number(req.params.doctorId) }).toArray((err, docs) => {
      if(err) res.status(500).end();

      res.status(200).send({	
        doctor: { patients: docs[0].patients }
      })

    })  
  
});


doctorRouter.use('/:doctorId/patients', patientRouter);


app.use('/patients', patientRouter);
app.use('/doctors', doctorRouter)

app.get('/role', function(req, res){
  //console.log(req.user)  
  
  res.status(200).send({ user : { role: req.user['http://localhost/roles'], id: req.user['http://localhost/id']} })

})



 MongoClient.connect(url, function(err,db){
   if(err)throw err;

   mongoDatabase = db;
  
   patients = mongoDatabase.collection('patients');
   doctors = mongoDatabase.collection('doctors');


	app.listen(3000, function(){
		console.log('Listening on port 3000');
	});

 })


