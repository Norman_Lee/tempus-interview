const Joi = require('joi');

module.exports = {
  body: {
    date: Joi.date().required(),
    description: Joi.string().required(),
    doctor_id: Joi.any().required(),
    doctorName: Joi.any().required()
  }
};
